#!/bin/bash

# Database information
DB_NAME=""
DB_USER=""
DB_PASSWORD=""
DB_HOST=""
DB_CHARSET=""
DB_COLLATE=""

# Site Domain. Example local.project-setup.com
SITE_DOMAIN=""

# WP Core Version
WP_VERSION="latest"

# Chose SQL file for DB Import
SQL_FILE=""

# Chose OS Type between: LINUX,WINDOWS,MACOS
OS_TYPE=""

# Chose Server Type between: APACHE,NGINX
SERVER_TYPE="" 

# User project owner. Example www-data:www-data
SERVER_OWNER=""

# Files permission 
FILES_PERMISSIONS=""

# Chose Server configuration file in witch a VirtualHost will be added. For example "/etc/apache2/apache2.conf"
# The SERVER_CONF and SERVER_CONF_DIR should not be set at the same time
SERVER_CONF=""

# Chose Server main Directory. For example "/etc/apache2/"
# The SERVER_CONF and SERVER_CONF_DIR should not be set at the same time
SERVER_CONF_DIR=""
