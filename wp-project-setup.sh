#!/bin/bash

# Declare some constants
CONFIG_FILE="config.sh"
WP_CONFIG_SAMPLE="wp-config-local-sample.php"
WP_CONFIG="wp-config.php"

# Get current dir
PROJECT_DIR=$PWD

# Get current folder
PROJECT_FOLDER=$(basename $PWD)

############ START Dependencies Checks ############

# Check if Config file exists
if ! test -f "$CONFIG_FILE"; then
  echo "$CONFIG_FILE does not exists"
  exit 1
fi

# Includes your config file
source $CONFIG_FILE

if [[ "$OS_TYPE" == 'WINDOWS' ]]; then
  WPCLI_USER=""
else
  WPCLI_USER="sudo -u $SUDO_USER "
  if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root"
    exit 1
  fi
fi

# Check if WP Config Local Sample file exists
if ! test -f "$WP_CONFIG_SAMPLE"; then
  echo "WP Config Local Sample: $WP_CONFIG_SAMPLE does not exists"
  exit 1
fi

# Check if WP CLI is installed
if ! command -v wp &>/dev/null; then
  echo "WP CLI could not be found"
  exit 1
fi

# Check if PHP is installed
if ! command -v php &>/dev/null; then
  echo "PHP could not be found"
  exit 1
fi

# Check if MYSQL is installed
if ! command -v mysql &>/dev/null; then
  echo "MYSQL could not be found"
  exit 1
fi

# Check if Git is installed
if ! command -v git &>/dev/null; then
  echo "Git could not be found"
  exit 1
fi

# Check if Database information is present
if test -z "$DB_NAME"; then
  echo "DB_NAME should not be empty"
  exit 1
fi
if test -z "$DB_USER"; then
  echo "DB_USER should not be empty"
  exit 1
fi
if test -z "$DB_PASSWORD"; then
  MYSQL_PASS=""
else
  MYSQL_PASS=" -p$DB_PASSWORD"
fi
if test -z "$DB_HOST"; then
  echo "DB_HOST should not be empty"
  exit 1
fi
if test -z "$DB_CHARSET"; then
  echo "DB_CHARSET should not be empty"
  exit 1
fi
if test -z "$SITE_DOMAIN"; then
  echo "SITE_DOMAIN should not be empty"
  exit 1
fi

# Check if DB Import file exists
if ! test -f "$SQL_FILE"; then
  echo "SQL File: $SQL_FILE does not exists"
  exit 1
fi

if [[ "$OS_TYPE" != 'LINUX' ]] && [[ "$OS_TYPE" != 'WINDOWS' ]] && [[ "$OS_TYPE" != 'MACOS' ]]; then
  echo "OS_TYPE should be LINUX,WINDOWS,MACOS"
  exit 1
fi

if [[ "$SERVER_TYPE" != 'APACHE' ]] && [[ "$SERVER_TYPE" != 'NGINX' ]]; then
  echo "SERVER_TYPE should be APACHE,NGINX"
  exit 1
fi

if ! test -z "$SERVER_CONF" && ! test -z "$SERVER_CONF_DIR"; then
  echo "The SERVER_CONF and SERVER_CONF_DIR should not be set at the same time, chose one"
  exit 1
fi

if ! test -z "$SERVER_CONF"; then

  # Check if Server conf file exists
  if ! test -f "$SERVER_CONF"; then
    echo "Server conf file: $SERVER_CONF does not exists"
    exit 1
  fi
elif ! test -z "$SERVER_CONF_DIR"; then

  # Check if Server conf directory exists
  if ! [ -d "${SERVER_CONF_DIR}sites-available/" ]; then
    echo "Server conf directory: ${SERVER_CONF_DIR}sites-available/ does not exists"
    exit 1
  fi
  if ! [ -d "${SERVER_CONF_DIR}sites-enabled/" ]; then
    echo "Server conf directory: ${SERVER_CONF_DIR}sites-enabled/ does not exists"
    exit 1
  fi
else
  echo "Provide Server configuration file or directory, with SERVER_CONF or SERVER_CONF_DIR"
  exit 1
fi

if ! test -z "$SERVER_CONF" && [[ "$SERVER_TYPE" == 'NGINX' ]]; then
  echo "NGINX currently is supported only with SERVER_CONF_DIR"
  exit 1
fi

# If WP Core version is not pointed, fallback to latest
if test -z "$WP_VERSION"; then
  WP_VERSION="latest"
fi

# Set proper project dir when using Cygwin in Windows
if [[ "$OS_TYPE" == 'WINDOWS' ]]; then
  PROJECT_DIR=$(cygpath -w $PROJECT_DIR)
fi
############ END Dependencies Checks ############

# Append VirtualHost setting in apache's main conf file of Linux and MAC OS
function append_apache_conf_linux_mascos() {
  cat <<EOF >>$SERVER_CONF
<VirtualHost *:80>
  DocumentRoot $PROJECT_DIR
  ServerName $SITE_DOMAIN
  ServerAlias $SITE_DOMAIN

  <Directory $PROJECT_DIR>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order allow,denyss
    allow from all
  </Directory>
</VirtualHost>
EOF
  echo "Success: Apache VirtualHost settings appended in '$SERVER_CONF'."
  sleep 2
}

# Append VirtualHost setting in apache's main conf file of Windows
function append_apache_conf_windows() {
  cat <<EOF >>$SERVER_CONF
<VirtualHost $SITE_DOMAIN:80>
        ServerName $SITE_DOMAIN

        ServerAdmin webmaster@localhost
        DocumentRoot "$PROJECT_DIR"

        <Directory "$PROJECT_DIR">
                AllowOverride All
                Require all granted
        </Directory>
</VirtualHost>
EOF

  echo "Success: Apache VirtualHost settings appended in '$SERVER_CONF'."
  sleep 2
}

# Create new conf file in sites-available with VirtualHost settings
function create_apache_conf_linux_mascos() {
  cat <<EOF >${SERVER_CONF_DIR}sites-available/$PROJECT_FOLDER.conf
<VirtualHost *:80>
  DocumentRoot $PROJECT_DIR
  ServerName $SITE_DOMAIN
  ServerAlias $SITE_DOMAIN

  <Directory $PROJECT_DIR>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order allow,deny
    allow from all
  </Directory>
</VirtualHost>
EOF

  echo "Success: Apache VirtualHost conf file created in '${SERVER_CONF_DIR}sites-available/$PROJECT_FOLDER.conf'."
  sleep 2
}

# Create new conf file in sites-available with VirtualHost settings
function create_nginx_conf() {
  cat <<EOF >${SERVER_CONF_DIR}sites-available/$PROJECT_FOLDER.conf
server {
     listen 80;
     listen 443 ssl;
     server_name $SITE_DOMAIN;
     root $PROJECT_DIR; # update this line, based on your localhost setup/location 
}
EOF

  echo "Success: Nginx VirtualHost conf file created in '${SERVER_CONF_DIR}sites-available/$PROJECT_FOLDER.conf'."
  sleep 2
}

# Copy wp-config-local-sample.php into wp-config.php
if ${WPCLI_USER}cp $WP_CONFIG_SAMPLE $WP_CONFIG; then
  echo "Success: '$WP_CONFIG_SAMPLE' copied into '$WP_CONFIG'."
  sleep 2
else
  echo "Error: '$WP_CONFIG_SAMPLE' coping into '$WP_CONFIG' failed."
  exit 1
fi

# Set the DB values in wp-config.php
if [[ "$OS_TYPE" == 'LINUX' ]] || [[ "$OS_TYPE" == 'WINDOWS' ]]; then
  sed -i "s/%%DB_NAME%%/${DB_NAME}/g" $WP_CONFIG
  sed -i "s/%%DB_USER%%/${DB_USER}/g" $WP_CONFIG
  sed -i "s/%%DB_PASSWORD%%/${DB_PASSWORD}/g" $WP_CONFIG
  sed -i "s/%%DB_HOST%%/${DB_HOST}/g" $WP_CONFIG
  sed -i "s/%%DB_CHARSET%%/${DB_CHARSET}/g" $WP_CONFIG
  sed -i "s/%%DB_COLLATE%%/${DB_COLLATE}/g" $WP_CONFIG
elif [[ "$OS_TYPE" == 'MACOS' ]]; then
  sed -i "" "s/%%DB_NAME%%/${DB_NAME}/g" $WP_CONFIG
  sed -i "" "s/%%DB_USER%%/${DB_USER}/g" $WP_CONFIG
  sed -i "" "s/%%DB_PASSWORD%%/${DB_PASSWORD}/g" $WP_CONFIG
  sed -i "" "s/%%DB_HOST%%/${DB_HOST}/g" $WP_CONFIG
  sed -i "" "s/%%DB_CHARSET%%/${DB_CHARSET}/g" $WP_CONFIG
  sed -i "" "s/%%DB_COLLATE%%/${DB_COLLATE}/g" $WP_CONFIG
fi

# Download WP Core
if ! ${WPCLI_USER}wp core download --skip-content --force --version="$WP_VERSION"; then
  echo "Error: WP Core download failed."
  exit 1
fi

# Drop DB if exists
mysql -u$DB_USER${MYSQL_PASS} -e "DROP DATABASE IF EXISTS \`$DB_NAME\`" &>/dev/null

# Create DB
if test -z "$DB_COLLATE"; then
  if mysql -u$DB_USER${MYSQL_PASS} -e "CREATE DATABASE \`$DB_NAME\` CHARACTER SET $DB_CHARSET" &>/dev/null; then
    echo "Success: Database '$DB_NAME' created."
    sleep 2
  else
    echo "Error: Database '$DB_NAME' creation failed."
    exit 1
  fi
else
  if mysql -u$DB_USER${MYSQL_PASS} -e "CREATE DATABASE \`$DB_NAME\` CHARACTER SET $DB_CHARSET COLLATE $DB_COLLATE" &>/dev/null; then
    echo "Success: Database '$DB_NAME' created."
    sleep 2
  else
    echo "Error: Database '$DB_NAME' creation failed."
    exit 1
  fi
fi

# Grant all privileges to the user
if mysql -u$DB_USER${MYSQL_PASS} -e "GRANT ALL PRIVILEGES ON \`$DB_NAME\`.* TO '$DB_USER'@'$DB_HOST'" &>/dev/null; then
  echo "Success: Permission for database granted to user '$DB_USER'."
  sleep 2
else
  echo "Error: Permission for database granting to user '$DB_USER' failed."
  exit 1
fi

# Import DB
if ! ${WPCLI_USER}wp db import "$SQL_FILE"; then
  echo "Error: SQL '$SQL_FILE' import failed."
  exit 1
fi

# Add Domain in hosts file
if [[ "$OS_TYPE" == 'LINUX' ]] || [[ "$OS_TYPE" == 'MACOS' ]]; then

  if echo "127.0.0.1 $SITE_DOMAIN" >>/etc/hosts; then
    echo "Success: Domain '$SITE_DOMAIN' added in hosts file '/etc/hosts'."
    sleep 2
  else
    echo "Error: Domain '$SITE_DOMAIN' adding in hosts file '/etc/hosts' failed."
    exit 1
  fi

elif [[ "$OS_TYPE" == 'WINDOWS' ]]; then

  echo "You should manually add '127.0.0.1 $SITE_DOMAIN' in C:\windows\system32\drivers\etc\hosts file"
  read -n 1 -s -r -p "When ready, Press any key to continue..."
  echo ""
fi

# If Server conf file is chosen
if ! test -z "$SERVER_CONF"; then

  if [[ "$SERVER_TYPE" == 'APACHE' ]]; then

    if [[ "$OS_TYPE" == 'LINUX' ]] || [[ "$OS_TYPE" == 'MACOS' ]]; then
      append_apache_conf_linux_mascos

    elif [[ "$OS_TYPE" == 'WINDOWS' ]]; then
      append_apache_conf_windows
    fi
  fi

# If Server conf dir is chosen
elif ! test -z "$SERVER_CONF_DIR"; then

  if [[ "$SERVER_TYPE" == 'APACHE' ]]; then

    if [[ "$OS_TYPE" == 'LINUX' ]] || [[ "$OS_TYPE" == 'MACOS' ]]; then
      create_apache_conf_linux_mascos

    elif [[ "$OS_TYPE" == 'WINDOWS' ]]; then
      create_apache_conf_windows
    fi

  elif [[ "$SERVER_TYPE" == 'NGINX' ]]; then
    create_nginx_conf
  fi

  # Remove old conf file if exists
  if test -f "${SERVER_CONF_DIR}sites-enabled/$PROJECT_FOLDER.conf"; then
    rm ${SERVER_CONF_DIR}sites-enabled/$PROJECT_FOLDER.conf
  fi

  # Activate Server conf
  if ln -s ${SERVER_CONF_DIR}sites-available/$PROJECT_FOLDER.conf ${SERVER_CONF_DIR}sites-enabled/$PROJECT_FOLDER.conf; then
    echo "Success: Server conf symbolic link created."
    sleep 2
  else
    echo "Error: Server conf symbolic link creation failed."
    exit 1
  fi
fi

# Restart Apache service
if [[ "$SERVER_TYPE" == 'APACHE' ]]; then

  if [[ "$OS_TYPE" == 'LINUX' ]]; then
    systemctl restart apache2
    echo "Success: Apache service restarted."
    sleep 2

  elif [[ "$OS_TYPE" == 'MACOS' ]]; then
    apachectl restart
    echo "Success: Apache service restarted."
    sleep 2

  else
    echo "You should manually restart your apache service"
    read -n 1 -s -r -p "When ready, Press any key to continue..."
    echo ""
  fi

# Restart Nginx service
elif [[ "$SERVER_TYPE" == 'NGINX' ]]; then

  if [[ "$OS_TYPE" == 'LINUX' ]]; then

    if systemctl restart nginx; then
      echo "Success: Nginx service restarted."
      sleep 2
    else
      echo "Error: Nginx service restart failed."
      exit 1
    fi

  elif
    [[ "$OS_TYPE" == 'MACOS' ]]
  then
    if nginx -s reload; then
      echo "Success: Nginx service restarted."
      sleep 2
    else
      echo "Error: Nginx service restart failed."
      exit 1
    fi

  else
    echo "You should manually restart your nginx service"
    read -n 1 -s -r -p "When ready, Press any key to continue..."
    echo ""
  fi
fi

# WP Install plugins
${WPCLI_USER}wp plugin install query-monitor be-media-from-production dx-localhost --skip-plugins

# Activate plugins
if ${WPCLI_USER}wp plugin activate query-monitor --network be-media-from-production dx-localhost --quiet --skip-plugins ||
  ${WPCLI_USER}wp plugin activate query-monitor be-media-from-production dx-localhost --quiet --skip-plugins; then

  echo "Success: Plugins activated."
  sleep 2
else
  echo "Error: Plugins activation failed."
  sleep 2
fi

# Runs the WordPress database update procedure
if ${WPCLI_USER}wp core update-db --network &>/dev/null || ${WPCLI_USER}wp core update-db --quiet; then
  echo "Success: WordPress database updated."
  sleep 2
else
  echo "Error: WordPress database update failed."
  exit 1
fi

# Update permalink structure
if ${WPCLI_USER}wp rewrite structure --skip-plugins $(${WPCLI_USER}wp option get permalink_structure --skip-plugins); then
  echo "Success: Updated permalink structure."
  sleep 2
else
  echo "Error: Permalink structure update failed."
  exit 1
fi

# Set owner for project
if ! test -z "$SERVER_OWNER"; then

  if chown -R $SERVER_OWNER $PROJECT_DIR; then
    echo "Success: Project owner set to '$SERVER_OWNER'." &&
      sleep 2
  else
    echo "Error: Failed to set project owner to '$SERVER_OWNER'." &&
      exit 1
  fi
fi

# Set files permissions for project
if ! test -z "$FILES_PERMISSIONS"; then

  if chmod -R $FILES_PERMISSIONS $PROJECT_DIR; then
    echo "Success: Project file permissions set to '$FILES_PERMISSIONS'."
    sleep 2
  else
    echo "Error: Failed to set project file permissions to '$FILES_PERMISSIONS'."
    exit 1
  fi
fi

# Discard file permission changes for git
if git config core.fileMode false; then
  echo "Success: Discarded file permission changes for git." &&
    sleep 2
fi

echo "Success: Setup is ready open project http://${SITE_DOMAIN}/"
