# WP project setup #

This is a bash script which will automate WP projects setup

## Needed dependencies*
* WP CLI
* PHP
* MySQL
* Git
* Apache/Nginx
* Cygwin (Only for Windows)
  
#### * All dependencies needs to be added and accessible in PATH variable


## How to use
1. Clone this repo locally
2. Make the ```wp-project-setup.sh``` file executable with ```chmod +x wp-project-setup.sh```
3. Make the 'wp-project-setup.sh' file runnable globally. For Linux and Mac OS, copy the 'wp-project-setup.sh' in /usr/local/bin directory
4. Clone the project repository, you are going to set up
5. In the project's root directory, copy ```config-sample.sh``` into ```config.sh```
6. Edit the options in ```config.sh``` as described in __Config options__ section
7. Run the ```wp-project-setup.sh``` script in the project's root directory with root permissions

## Config options
1. Database information
    * __DB_NAME__ - The name of the database, witch will be created
    * __DB_USER__ - The name of the DB user, witch will be used
    * __DB_PASSWORD__ - The password for the DB user, witch will be used
    * __DB_HOST__ - The DB Host, witch will be used
    * __DB_CHARSET__ - The DB charset, witch will be used
    * __DB_COLLATE__ - The DB charset-collate, witch will be used, it can be empty
    
2. __SITE_DOMAIN__ - The project's site domain. Example: local.project-setup.com
3. __WP_VERSION__ - The WP Core version, witch will be downloaded
4. __SQL_FILE__ - The SQL, witch will be imported into the DB
5. __OS_TYPE__ - The OS type on which the script is running, choose between: LINUX,WINDOWS,MACOS
6. __SERVER_TYPE__ - The Server type on which the project will run, choose between: APACHE,NGINX
7. __SERVER_OWNER__ - Set the project owner, it can be empty. Example: www-data:www-data
8. __FILES_PERMISSIONS__ - Set the project file permissions, it can be empty. Example: 777
9. __SERVER_CONF__* - Server configuration file in which a VirtualHost will be added. Example: /etc/apache2/apache2.conf
10. __SERVER_CONF_DIR__* - Choose Server main Directory. Example: /etc/apache2/
#### * The SERVER_CONF and SERVER_CONF_DIR should not be set at the same time, chose only one


## How to use inside project repository ( For Project Owners )
* You should have ```config-sample.sh``` in your project's root directory
* You should have ```wp-config-local-sample.php``` in your project's root directory. Keep in mind that "Database Configuration" section from the file is important to NOT be changed, because we're replacing the strings there
* The team should have SQL file for importing